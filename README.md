### Example


Exemplo de uma API RestFul usando python e flask.


O exemplo é bem idiota, mas simplifica a ideia.

A API tem métodos CREATE/GET/UPDATE/DELETE/ e, no caso, cria um interface com um "banco" que armazena as informações.

- Clone o repositório
``` shell
$ git clone https://gitlab.com/wallyssonlima/simple-api.git
```

- Instale as dependêcias
``` shell
$ python3 -m venv env
$ source env/bin/activate
$ cd simple-api
$ pip install -r requirements.txt
```

- Test
``` shell
$ python app.py
```

- Create
``` shell
curl -i -H "Content-Type: application/json" -X POST -d '{"id": 5, "name": "Paulo Ricardo", "age": 17}' http://localhost:5000/api/v1.0/peoples
``` 


- GET
``` shell
# Um pessoa
$ curl http://localhost:5000/api/v1.0/people/{id}
```

``` shell
# todas as pessoas
$ curl http://localhost:5000/api/v1.0/peoples/
```